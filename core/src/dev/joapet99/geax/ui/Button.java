package dev.joapet99.geax.ui;

import com.badlogic.gdx.audio.Music.OnCompletionListener;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Filter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Button class based on {@link com.badlogic.gdx.scenes.scene2d.ui.TextButton TextButton}.
 * The difference is that you don't need to provide any textures or skins to this button to get it to show a border.
 * @author Joakim Pettersvold
 *
 */
public class Button extends TextButton{
	Label buttonLabel;
	
	Texture upTex, downTex, checkedTex;
	
	public Button(String text) {
		super(text, UIDefaults.getButtonStyle());
	}
	
	/**
	 * Creates the pixmap textures for the button
	 * @param r - rectangle to base the textures on
	 */
	private void createTextures(float fWidth, float fHeight) {
		int width = Math.round(fWidth);
		int height = Math.round(fHeight);
		
		Pixmap tmp = new Pixmap(width, height, Pixmap.Format.RGB888);
		tmp.setFilter(Filter.NearestNeighbour);
		
		tmp.setColor(Color.BLACK);
		tmp.fill();
		
		tmp.setColor(Color.WHITE);
		tmp.drawRectangle(0, 0, width, height);
		
		if(this.getStyle().up != null && this.getStyle().up instanceof TextureRegionDrawable) {
			((TextureRegionDrawable)this.getStyle().up).getRegion().getTexture().dispose();
		}
		this.getStyle().up = new TextureRegionDrawable(new Texture(tmp));
		
		tmp.setColor(Color.GRAY);
		tmp.fill();
		
		tmp.setColor(Color.WHITE);
		tmp.drawRectangle(0, 0, width, height);
		
		if(this.getStyle().down != null && this.getStyle().down instanceof TextureRegionDrawable) {
			((TextureRegionDrawable)this.getStyle().down).getRegion().getTexture().dispose();
		}
		this.getStyle().down = new TextureRegionDrawable(new Texture(tmp));
		
		tmp.setColor(Color.DARK_GRAY);
		tmp.fill();
		
		tmp.setColor(Color.WHITE);
		tmp.drawRectangle(0, 0, width, height);
		
		if(this.getStyle().checked != null && this.getStyle().checked instanceof TextureRegionDrawable) {
			((TextureRegionDrawable)this.getStyle().checked).getRegion().getTexture().dispose();
		}
		this.getStyle().checked = new TextureRegionDrawable(new Texture(tmp));
		
		tmp.dispose();
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}
	@Override
	public void act(float delta) {
		super.act(delta);
	}
	
	@Override
	protected void sizeChanged() {
		if(this.getWidth() > 0 && this.getHeight() > 0) {
			createTextures(this.getWidth(), this.getHeight());
		}
		super.sizeChanged();
	}
}
