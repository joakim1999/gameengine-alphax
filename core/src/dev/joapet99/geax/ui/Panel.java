package dev.joapet99.geax.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Filter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Panel extends Table{
	public void createTextures(float fWidth, float fHeight) {
		int width = Math.round(fWidth);
		int height = Math.round(fHeight);
		
		Pixmap tmp = new Pixmap(width, height, Pixmap.Format.RGB888);
		tmp.setFilter(Filter.NearestNeighbour);
		
		tmp.setColor(Color.BLACK);
		tmp.fill();
		
		tmp.setColor(Color.WHITE);
		tmp.drawRectangle(0, 0, width, height);
		
		if(this.getBackground() != null && this.getBackground() instanceof TextureRegionDrawable) {
			((TextureRegionDrawable)this.getBackground()).getRegion().getTexture().dispose();
		}
		
		this.setBackground(new TextureRegionDrawable(new Texture(tmp)));
		
		tmp.dispose();
	}
	
	@Override
	protected void sizeChanged() {
		if(this.getWidth() > 0 && this.getHeight() > 0) {
			createTextures(this.getWidth(), this.getHeight());
		}
		super.sizeChanged();
	}
}
