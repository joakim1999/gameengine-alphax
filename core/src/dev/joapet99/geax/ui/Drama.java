package dev.joapet99.geax.ui;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import dev.joapet99.geax.action.RepeatDelayAction;

public class Drama extends Label{
	RepeatDelayAction repeatAction;
	
	ArrayList<String> pieces;
	int pieceNumber;
	String currentPiece;

	public Drama() {
		super("", UIDefaults.getLabelStyle());
		init();
	}
	
	public Drama(ArrayList<String> pieces) {
		super("", UIDefaults.getLabelStyle());
		init();
		
		this.pieces = pieces;
		this.pieceNumber = 0;
	}
	
	private void init() {
		this.setWrap(true);
		
		Action writeAction = new Action() {
			int index = 0;
			@Override
			public boolean act(float delta) {
				if(index < currentPiece.length()) {
					Label l = (Label)this.getActor();
					l.setText(l.getText() + String.valueOf(currentPiece.charAt(index)));
					index++;
					if(index < currentPiece.length()) return false;
				}
				return true;
			}
			
			@Override
			public void restart() {
				super.restart();
				index = 0;
			}
		};
		
		this.repeatAction = new RepeatDelayAction();
		this.repeatAction.setAction(writeAction);
		this.repeatAction.setDuration(0.01f);
		this.repeatAction.setCount(1);
	}
	
	public void startDrama() {
		this.setVisible(true);
		startPiece();
	}
	
	public void restartDrama() {
		this.setVisible(true);
		this.pieceNumber = 0;
		repeatLast();
	}
	
	public void proceed() {
		if(!this.getActions().contains(repeatAction, false)) {
			if(this.pieceNumber >= this.pieces.size() - 1) {
				this.setVisible(false);
			}
			nextPiece();
			startPiece();
		}
		else {
			finishPiece();
		}
	}
	
	public void repeatLast() {
		if(!this.getActions().contains(this.repeatAction, false)) {
			startPiece();
		}else {
			restartPiece();
		}
	}
	
	public void jumpBack() {
		previousPiece();
		if(!this.getActions().contains(repeatAction, false)) {
			showCurrentPiece();
		}
		else {
			this.repeatAction.terminate();
			showCurrentPiece();
		}
	}
	
	public void jumpForward() {
		nextPiece();
		if(!this.getActions().contains(this.repeatAction, false)) {
			showCurrentPiece();
		}
		else {
			this.repeatAction.terminate();
			showCurrentPiece();
		}
	}
	
	void startPiece() {
		this.addAction(this.repeatAction);
		restartPiece();
	}
	
	void restartPiece() {
		this.repeatAction.restart();
		this.currentPiece = this.pieces.get(this.pieceNumber);
		this.setText("");
	}
	
	void previousPiece() {
		if(pieceNumber == 0) return;
		this.pieceNumber--;
	}
	
	void nextPiece() {
		if(pieceNumber >= pieces.size() - 1) return;
		this.pieceNumber++;
	}
	
	void showPiece(int pieceNumber) {
		if(pieceNumber >= pieces.size() - 1) return;
		this.currentPiece = this.pieces.get(this.pieceNumber);
		this.setText(this.currentPiece);
	}
	
	void showCurrentPiece() {
		this.currentPiece = this.pieces.get(this.pieceNumber);
		this.setText(this.currentPiece);
	}
	
	void finishPiece() {
		this.repeatAction.terminate();
		this.setText(this.currentPiece);
	}
	
	public void setPieces(ArrayList<String> pieces){
		this.pieces = pieces;
	}
	
	public ArrayList<String> getPieces(){
		return this.pieces;
	}
	
	public void setTypeDelay(float delay) {
		this.repeatAction.setDuration(delay);
	}
}
