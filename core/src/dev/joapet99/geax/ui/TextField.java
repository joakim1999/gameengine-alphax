package dev.joapet99.geax.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Filter;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class TextField extends com.badlogic.gdx.scenes.scene2d.ui.TextField{

	public TextField(String text) {
		super(text, new TextFieldStyle(UIDefaults.getFont(), Color.WHITE, null, null, null));
	}
	
	private void createTextures(float fWidth, float fHeight) {
		int width = Math.round(fWidth);
		int height = Math.round(fHeight);
		
		//Create background for the text field
		Pixmap tmp = new Pixmap(width, height, Pixmap.Format.RGB888);
		tmp.setFilter(Filter.NearestNeighbour);
		
		tmp.setColor(Color.BLACK);
		tmp.fill();
		
		tmp.setColor(Color.WHITE);
		tmp.drawRectangle(0, 0, width, height);
		
		TextFieldStyle style = this.getStyle();
		
		if(style.background != null && style.background instanceof TextureRegionDrawable) {
			((TextureRegionDrawable)style.background).getRegion().getTexture().dispose();
		}
		
		style.background = new TextureRegionDrawable(new Texture(tmp));
		
		tmp.dispose();
		
		//Create cursor and selection textures for the text field if they don't exist
		tmp = new Pixmap(5, 20, Format.RGB888);
		if(style.cursor == null) {
			tmp.setColor(Color.WHITE);
			tmp.fill();
			
			style.cursor = new TextureRegionDrawable(new Texture(tmp));
			
			if(style.selection == null) {
				style.selection = new TextureRegionDrawable(new Texture(tmp));
			}
		}
		
		tmp.dispose();
	}
	
	@Override
	protected void sizeChanged() {
		if(this.getWidth() > 0 && this.getHeight() > 0) {
			createTextures(this.getWidth(), this.getHeight());
		}
		super.sizeChanged();
	}
}
