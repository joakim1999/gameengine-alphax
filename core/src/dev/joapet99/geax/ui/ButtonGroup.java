package dev.joapet99.geax.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class ButtonGroup extends Group{
	boolean topAnchor = false;
	boolean rightAnchor = false;
	
	boolean row = false;
	
	float standardWidth = -1;
	float standardHeight = -1;
	
	/**
	 * Recalculates the button positions and sizes for this ButtonGroup
	 */
	public void recalculate() {
		int size = this.getChildren().size;
		
		float x, y, width, height;
		width = standardWidth;
		height = standardHeight;
		
		x = rightAnchor && width != -1 ? -width : 0;
		y = topAnchor && height != -1 ? -height : 0;
		
		if(row)
		{
			width = width < 0 ? this.getWidth() / size : width;
			height = height < 0 ? this.getHeight() : height;
			
			if(rightAnchor && standardWidth != -1)
			{
				//Buttons will extend to the left if standardwidth is set
				for(int i = size - 1; i >= 0; i--) {
					Actor a = this.getChildren().get(i);
					if(a instanceof Button) {
						a.setBounds(x, y, width, height);
						x -= width;
					}
					else {
						System.err.println(this.toString() + ": " + a.toString() + " is not a button");
					}
				}
			}
			else
			{
				//Buttons will extend to the right if standardwidth is set
				for(Actor a : this.getChildren()) {
					if(a instanceof Button) {
						a.setBounds(x, y, width, height);
						x += width;
					}
					else {
						System.err.println(this.toString() + ": " + a.toString() + " is not a button");
					}
				}
			}
		}
		else
		{
			width = width < 0 ? this.getWidth() : width;
			height = height < 0 ? this.getHeight() / size : height;
			
			if(topAnchor && standardHeight != -1)
			{
				//Buttons will extend downwards if standardheight is set
				for(Actor a : this.getChildren())
				{
					if(a instanceof Button)
					{
						a.setBounds(x, y, width, height);
						y -= height;
					}
					else
					{
						System.err.println(this.toString() + ": " + a.toString() + " is not a button");
					}
				}
			}
			else {
				//Buttons will extend upwards if standardheight is set
				for(int i = size - 1; i >= 0; i--) {
					Actor a = this.getChildren().get(i);
					if(a instanceof Button) {
						a.setBounds(x, y, width, height);
						y += height;
					}
					else {
						System.err.println(this.toString() + ": " + a.toString() + " is not a button");
					}
				}
			}
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}
	
	public void setStandardWidth(float width) {
		this.standardWidth = width;
		this.recalculate();
	}
	
	public float getStandardWidth() {
		return this.standardWidth;
	}
	
	public void setStandardHeight(float height) {
		this.standardHeight = height;
		this.recalculate();
	}
	
	public float getStandardHeight() {
		return this.standardHeight;
	}
	
	public void setAnchors(boolean top, boolean right)
	{
		this.topAnchor = top;
		this.rightAnchor = right;
		this.recalculate();
	}
	
	public boolean[] getAnchors()
	{
		return new boolean[]{topAnchor, rightAnchor};
	}
	
	public void setRow(boolean row)
	{
		this.row = row;
	}
	
	public boolean isRow()
	{
		return this.row;
	}
	
	@Override
	protected void childrenChanged() {
		this.recalculate();
		super.childrenChanged();
	}
	
	@Override
	protected void positionChanged() {
		//this.recalculate();
		super.positionChanged();
	}

	@Override
	protected void sizeChanged() {
		this.recalculate();
		super.sizeChanged();
	}
}
