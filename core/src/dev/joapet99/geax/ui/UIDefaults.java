package dev.joapet99.geax.ui;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class UIDefaults {
	private static boolean created = false;
	
	private static BitmapFont font;
	private static TextButtonStyle buttonStyle;
	private static LabelStyle labelStyle;
	
	private static void create() {
		UIDefaults.font = new BitmapFont();
		
		UIDefaults.buttonStyle = new TextButtonStyle();
		UIDefaults.buttonStyle.font = font;
		
		UIDefaults.labelStyle = new LabelStyle();
		UIDefaults.labelStyle.font = font;
		
		UIDefaults.created = true;
	}
	
	public static boolean isCreated() {
		return created;
	}
	
	public static BitmapFont getFont() {
		if(!isCreated()) {
			create();
		}
		return UIDefaults.font;
	}
	
	public static void setFont(BitmapFont f) {
		if(!isCreated()) {
			create();
		}
		UIDefaults.font = f;
	}
	
	public static TextButtonStyle getButtonStyle() {
		if(!isCreated()) {
			create();
		}
		return UIDefaults.buttonStyle;
	}
	
	public static void setButtonStyle(TextButtonStyle style) {
		if(!isCreated()) {
			create();
		}
		UIDefaults.buttonStyle = style;
	}
	
	public static LabelStyle getLabelStyle() {
		if(!isCreated()) {
			create();
		}
		return UIDefaults.labelStyle;
	}
	
	public static void setLabelStyle(LabelStyle style) {
		if(!isCreated()) {
			create();
		}
		UIDefaults.labelStyle = style;
	}
}
