package dev.joapet99.geax.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

public class Console extends Panel{
	Panel output;
	TextField input;
	
	Label template;
	
	Label.LabelStyle style;
	
	int outputLines = 0;
	
	boolean a = true;
	
	//TODO Create textures for the components. Create a derived TextField class?
	
	public Console() {
		output = new Panel();
		output.align(Align.topLeft);
		this.addActor(output);
		
		input = new TextField("");
		this.addActor(input);
		input.setHeight(50);
		
		style = new Label.LabelStyle(UIDefaults.getFont(), Color.WHITE);
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
			System.out.println("Enter-ed");
			process(input.getText());
		}
	}

	public void process(String text) {
		if(!text.isEmpty()) {
			Label l = new Label(text, style);
			output.add(l).align(Align.left);
			output.row();
		}
	}
	
	@Override
	protected void sizeChanged() {
		input.setWidth(this.getWidth());
		
		output.setWidth(this.getWidth());
		output.setHeight(this.getHeight() - input.getHeight() + 1);
		output.setY(input.getHeight() - 1);
		super.sizeChanged();
	}
}
