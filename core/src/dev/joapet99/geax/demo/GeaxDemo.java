package dev.joapet99.geax.demo;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.*;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.*;
import com.badlogic.gdx.utils.Align;

import dev.joapet99.geax.action.RepeatDelayAction;
import dev.joapet99.geax.ui.Button;
import dev.joapet99.geax.ui.ButtonGroup;
import dev.joapet99.geax.ui.Drama;
import dev.joapet99.geax.ui.Panel;
import dev.joapet99.geax.ui.UIDefaults;

public class GeaxDemo extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	
	Button button;
	Stage stage;
	
	TextButtonStyle buttonStyle;
	Label someLabel;
	Drama someDrama;
	
	Image texTest;
	
	ButtonGroup grp;
	
	Rectangle btnRect;
	
	Panel panel;
	
	final boolean buttonGroupStandalone = true;
	
	@Override
	public void create () {
		Gdx.graphics.setTitle("GameEngine AlphaX - 0.0.1");
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		batch = new SpriteBatch();
		
		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add("Hello\nMy name is joakim");
		pieces.add("Hello. Lorem Ipsum Dipsum");
		pieces.add("Ripsum Klipsum Mipsum Nipsum Sipsum");
		
		someDrama = new Drama(pieces);
		someDrama.setAlignment(Align.topLeft);
		someDrama.setBounds(300, 100, 200, 500);
		someDrama.setText("Hell");
		
		btnRect = new Rectangle(0, 0, 200, 200);
		
		grp = new ButtonGroup();
		grp.setBounds(btnRect.getX(), btnRect.getY(), btnRect.getWidth(), btnRect.getHeight());
		// If you want to test the standardWidth
		grp.setStandardWidth(100);
		// If you want to test the standardHeight
		grp.setStandardHeight(100);
		
		button = new Button("Test");
		button.setBounds(btnRect.getX(), btnRect.getY(), btnRect.getWidth(), btnRect.getHeight());
		grp.addActor(button);
		
		button = new Button("Test2");
		button.setBounds(btnRect.getX() + 200, btnRect.getY(), btnRect.getWidth(), btnRect.getHeight());
		grp.addActor(button);
		
		button = new Button("Test3");
		button.setBounds(btnRect.getX() + 400, btnRect.getY(), btnRect.getWidth(), btnRect.getHeight());
		grp.addActor(button);
		
		grp.recalculate();
		
		panel = new Panel();
		panel.setBounds(0, 0, 250, 250);
		if(!buttonGroupStandalone) {
			panel.add(grp);
			stage.addActor(panel);
		}
		else {
			stage.addActor(grp);
		}
		stage.addActor(someDrama);
		someDrama.startDrama();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
		if(Gdx.input.isKeyJustPressed(Input.Keys.C)) {
			button = new Button("Testx");
			button.setBounds(btnRect.getX() + 400, btnRect.getY(), btnRect.getWidth(), btnRect.getHeight());
			grp.addActor(button);
		}
		
		if(Gdx.input.isKeyJustPressed(Input.Keys.M)) {
			if(!grp.isRow() && !grp.getAnchors()[1] && !grp.getAnchors()[0]) {
				grp.setAnchors(true, false);
			}
			else if(!grp.isRow() && !grp.getAnchors()[1] && grp.getAnchors()[0]) {
				grp.setAnchors(false, true);
			}
			else if(!grp.isRow() && grp.getAnchors()[1] && !grp.getAnchors()[0])
			{
				grp.setAnchors(true, true);
			}
			else if(!grp.isRow() && grp.getAnchors()[1] && grp.getAnchors()[0])
			{
				grp.setRow(true);
				grp.setAnchors(false, false);
			}
			else if(grp.isRow() && !grp.getAnchors()[1] && !grp.getAnchors()[0]) {
				grp.setAnchors(true, false);
			}
			else if(grp.isRow() && !grp.getAnchors()[1] && grp.getAnchors()[0]) {
				grp.setAnchors(false, true);
			}
			else if(grp.isRow() && grp.getAnchors()[1] && !grp.getAnchors()[0])
			{
				grp.setAnchors(true, true);
			}
			else if(grp.isRow() && grp.getAnchors()[1] && grp.getAnchors()[0])
			{
				grp.setRow(false);
				grp.setAnchors(false, false);
			}
			
			if(buttonGroupStandalone)
				grp.setPosition(grp.getAnchors()[1] ? 1280 : 0, grp.getAnchors()[0] ? 720 : 0);
		}
		
		if(Gdx.input.isKeyJustPressed(Input.Keys.R)) {
			grp.recalculate();
		}
		
		if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
			panel.setBounds(panel.getX(), panel.getY(), panel.getWidth(), panel.getHeight() + 1);
			grp.setBounds(grp.getX(), grp.getY(), grp.getWidth(), grp.getHeight() + 1);
			if(buttonGroupStandalone)
				grp.setBounds(grp.getX(), grp.getY() + 1, grp.getWidth(), grp.getHeight());
		}
		if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			panel.setBounds(panel.getX(), panel.getY(), panel.getWidth(), panel.getHeight() - 1);
			grp.setBounds(grp.getX(), grp.getY(), grp.getWidth(), grp.getHeight() - 1);
			if(buttonGroupStandalone)
				grp.setBounds(grp.getX(), grp.getY() - 1, grp.getWidth(), grp.getHeight());
		}
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			panel.setBounds(panel.getX(), panel.getY(), panel.getWidth() + 1, panel.getHeight());
			grp.setBounds(grp.getX(), grp.getY(), grp.getWidth() + 1, grp.getHeight());
			if(buttonGroupStandalone)
				grp.setBounds(grp.getX() + 1, grp.getY(), grp.getWidth(), grp.getHeight());
		}
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			panel.setBounds(panel.getX(), panel.getY(), panel.getWidth() - 1, panel.getHeight());
			grp.setBounds(grp.getX(), grp.getY(), grp.getWidth() - 1, grp.getHeight());
			if(buttonGroupStandalone)
				grp.setBounds(grp.getX() - 1, grp.getY(), grp.getWidth(), grp.getHeight());
		}
		
		if(Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
			someDrama.proceed();
		}
		
		if(Gdx.input.isButtonJustPressed(Input.Buttons.MIDDLE)) {
			someDrama.restartDrama();
		}
		
		if(Gdx.input.isButtonJustPressed(Input.Buttons.RIGHT)) {
			someDrama.repeatLast();
		}
		
		if(Gdx.input.isKeyJustPressed(Input.Keys.A)) {
			someDrama.jumpBack();
		}
		
		if(Gdx.input.isKeyJustPressed(Input.Keys.D)) {
			someDrama.jumpForward();
		}
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		stage.dispose();
	}
}
