package dev.joapet99.geax.demo;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import dev.joapet99.geax.ui.Console;
import dev.joapet99.geax.ui.Panel;
import dev.joapet99.geax.ui.UIDefaults;

public class ConsoleDemo extends ApplicationAdapter{
	Stage stage;
	
	Panel main;
	Panel output;
	TextField input;
	
	Console c;
	
	@Override
	public void create () {
		stage = new Stage();
		
		Gdx.input.setInputProcessor(stage);
		
		/*
		Pixmap pix = new Pixmap(600, 400, Format.RGB888);
		pix.setColor(Color.WHITE);
		pix.fill();
		
		Texture background = new Texture(pix);
		pix.dispose();
		
		pix = new Pixmap(5, 20, Format.RGB888);
		pix.setColor(Color.WHITE);
		pix.fill();
		
		Texture cur = new Texture(pix);
		pix.dispose();
		
		pix = new Pixmap(1, 1, Format.RGB888);
		pix.setColor(Color.GRAY);
		pix.fill();
		
		Texture sel = new Texture(pix);
		pix.dispose();
		
		main = new Panel();
		main.setBounds(300, 200, 600, 400);
		
		output = new Panel();
		main.addActor(output);
		output.setBounds(0, 50, 600, 350);
		
		input = new TextField("", new TextField.TextFieldStyle(new BitmapFont(), Color.WHITE, new TextureRegionDrawable(cur), new TextureRegionDrawable(sel),null new TextureRegionDrawable(background)));
		main.addActor(input);
		input.setBounds(10, 0, 580, 50);
		*/
		
		c = new Console();
		stage.addActor(c);
		c.setBounds(0, 0, 500, 500);
		
		//stage.addActor(main);
	}
	
	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	@Override
	public void dispose () {
		stage.dispose();
	}
}
