package dev.joapet99.geax.action;

import com.badlogic.gdx.scenes.scene2d.actions.DelegateAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;

public class RepeatDelayAction extends DelegateAction{
	static public final int FOREVER = -1;

	private int repeatCount, executedCount;
	private boolean finished;
	private boolean terminate;
	
	private float duration, time;

	protected boolean delegate (float delta) {
		if (executedCount == repeatCount) return true;
		
		time += delta;
		if (time < duration) return false;
		
		delta = time - duration;
		time = delta;
		
		if (action.act(delta)) {
			if (finished) return true;
			if (repeatCount > 0) executedCount++;
			if (executedCount == repeatCount) return true;
			if (action != null) action.restart();
		}
		return false;
	}

	/** Causes the action to not repeat again. */
	public void finish () {
		finished = true;
	}
	
	/** Breaks the loop **/
	public void terminate () {
		executedCount = repeatCount;
	}

	public void restart () {
		super.restart();
		executedCount = 0;
		time = 0;
		finished = false;
	}

	/** Sets the number of times to repeat. Can be set to {@link #FOREVER}. */
	public void setCount (int count) {
		this.repeatCount = count;
	}

	public int getCount () {
		return repeatCount;
	}
	
	/** Gets the time spent waiting for the delay. */
	public float getTime () {
		return time;
	}

	/** Sets the time spent waiting for the delay. */
	public void setTime (float time) {
		this.time = time;
	}

	public float getDuration () {
		return duration;
	}

	/** Sets the length of the delay in seconds. */
	public void setDuration (float duration) {
		this.duration = duration;
	}
}
